package cms.order.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmsOrderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmsOrderServiceApplication.class, args);
	}

}
